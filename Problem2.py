def compute():
	ans = 0
	fib1 = 1 
	fib2 = 2  
	while fib1 <= 4000000:
		if fib1 % 2 == 0:
			ans += fib1
		fib1, fib2 = fib2, fib1 + fib2
	return str(ans)


if __name__ == "__main__":
	print(compute())
